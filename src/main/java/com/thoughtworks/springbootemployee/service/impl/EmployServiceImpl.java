package com.thoughtworks.springbootemployee.service.impl;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.EmployService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployServiceImpl implements EmployService {
    public List<Employee> employeeList = new ArrayList<>(Arrays.asList(
            new Employee(1L, "aaa", 20, "female", 2.0,1L),
            new Employee(2L, "aaa", 20, "female", 2.0,2L),
            new Employee(3L, "aaa", 20, "female", 2.0,3L),
            new Employee(4L, "aaa", 20, "male", 2.0,4L)));

    @Override
    public List<Employee> getAllEmployees() {
        return employeeList;
    }

    @Override
    public Employee getEmployee(Long id) {
        return employeeList.stream().filter(
                employee -> employee.getId().equals(id)
        ).findFirst().orElse(null);
    }

    @Override
    public List<Employee> getEmployeesByGender(String gender) {
        return employeeList.stream().filter(
                employee -> employee.getGender().equals(gender)
        ).collect(Collectors.toList());
    }

    @Override
    public void createEmployee(Employee employee) {
        employee.setId(nextId());
        employeeList.add(employee);
    }

    private Long nextId() {
        return employeeList.stream()
                .map(Employee::getId)
                .max((o1, o2) -> (int) (o1 - o2))
                .orElse(0L) + 1;
    }

    @Override
    public void updateEmployee(Employee employee) {
        employeeList.stream()
                .filter(employee1 -> employee1.getId().equals(employee.getId()))
                .findFirst()
                .ifPresent(originEmployee -> {
                    originEmployee.setAge(employee.getAge() == null ? originEmployee.getAge() : employee.getAge());
                    originEmployee.setGender(employee.getGender() == null ? originEmployee.getGender() : employee.getGender());
                    originEmployee.setName(employee.getName() == null ? originEmployee.getName() : employee.getName());
                    originEmployee.setSalary(employee.getSalary() == null ? originEmployee.getSalary() : employee.getSalary());
                });
    }

    @Override
    public void deleteEmployee(Long id) {
        employeeList = employeeList.stream().filter(employee -> !employee.getId().equals(id)).collect(Collectors.toList());
    }


    @Override
    public List<Employee> pageQueryEmployees(Integer page, Integer size) {
        return employeeList.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }
}
