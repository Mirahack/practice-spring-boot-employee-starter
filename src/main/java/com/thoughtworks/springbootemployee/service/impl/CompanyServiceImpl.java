package com.thoughtworks.springbootemployee.service.impl;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import com.thoughtworks.springbootemployee.service.EmployService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements CompanyService {

    public List<Company> companyList = new ArrayList<>(Arrays.asList(
            new Company(1L, "a"),
            new Company(2L, "b"),
            new Company(3L, "c"),
            new Company(4L, "d")));

    @Autowired
    private EmployService employService;

    @Override
    public List<Company> getAllCompanies() {
        return companyList;
    }

    @Override
    public Company getCompany(Long id) {
        return companyList.stream().filter(company -> company.getId().equals(id)).findFirst().orElse(null);
    }

    @Override
    public List<Company> pageQueryCompanies(Integer page, Integer size) {
        return companyList.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }

    @Override
    public void addCompany(Company company) {
        company.setId(nextId());
        companyList.add(company);
    }

    @Override
    public void updateCompany(Company company) {
        companyList.stream()
                .filter(company1 -> company1.getId().equals(company.getId()))
                .findFirst()
                .ifPresent(originCompany -> originCompany.setName(company.getName() == null ? originCompany.getName() : company.getName()));
    }

    @Override
    public void deleteCompany(Long id) {
        companyList = companyList.stream().filter(company -> !company.getId().equals(id)).collect(Collectors.toList());
    }

    @Override
    public List<Employee> getEmployeesUnderCompanies(Long id) {
        return employService.getAllEmployees().stream().filter(employee -> employee.getCompanyId().equals(id)).collect(Collectors.toList());
    }

    private Long nextId() {
        return companyList.stream()
                .map(Company::getId)
                .max((o1, o2) -> (int) (o1 - o2))
                .orElse(0L) + 1;
    }


}
