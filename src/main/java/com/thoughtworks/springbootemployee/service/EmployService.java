package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.List;

public interface EmployService {

    public List<Employee> getAllEmployees();

    public Employee getEmployee(Long id);

    public List<Employee> getEmployeesByGender(String gender);

    public void createEmployee(Employee employee);

    public void updateEmployee(Employee employee);

    public void deleteEmployee(Long id);

    public List<Employee> pageQueryEmployees(Integer page, Integer size);
}
