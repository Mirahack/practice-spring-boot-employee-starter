package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.List;

public interface CompanyService {
    List<Company> getAllCompanies();

    Company getCompany(Long id);

    List<Company> pageQueryCompanies(Integer page, Integer size);

    void addCompany(Company company);

    void updateCompany(Company company);

    void deleteCompany(Long id);

    List<Employee> getEmployeesUnderCompanies(Long id);
}
