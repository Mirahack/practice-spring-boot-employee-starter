package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("companies")
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @GetMapping
    public List<Company> getAllCompanies() {
        return companyService.getAllCompanies();
    }

    @GetMapping("{id}")
    public Company getCompany(@PathVariable("id") Long id) {
        return companyService.getCompany(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> pageQueryCompanies(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        return companyService.pageQueryCompanies(page, size);
    }

    @GetMapping("{id}/employees")
    public List<Employee> getEmployeesUnderCompanies(@PathVariable("id") Long id) {
        return companyService.getEmployeesUnderCompanies(id);
    }

    @PostMapping
    public void addCompany(@RequestBody Company company) {
        companyService.addCompany(company);
    }

    @PutMapping
    public void updateCompany(@RequestBody Company company) {
        companyService.updateCompany(company);
    }

    @DeleteMapping("{id}")
    public void deleteCompany(@PathVariable("id") Long id) {
        companyService.deleteCompany(id);
    }

}
