package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.EmployService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private EmployService employService;

    @GetMapping()
    public List<Employee> getAllEmployees() {
        return employService.getAllEmployees();
    }

    @GetMapping("/{id}")
    public Employee getEmployee(@PathVariable("id") Long id) {
        return employService.getEmployee(id);
    }

    @GetMapping(params = {"gender"})
    public List<Employee> getEmployeeByGender(@RequestParam("gender") String gender) {
        return employService.getEmployeesByGender(gender);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public void createEmployee(@RequestBody Employee employee) {
        employService.createEmployee(employee);
    }

    @PutMapping()
    public void updateEmployee(@RequestBody Employee employee) {
        employService.updateEmployee(employee);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable("id") Long id) {
        employService.deleteEmployee(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> pageQueryEmployees(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        return employService.pageQueryEmployees(page, size);
    }

}
