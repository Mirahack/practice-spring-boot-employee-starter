O:Today we learned about HTTP and Restful. In addition, we also learned about pair programming and applied it to the learning of spring boot. In this programming approach, only one person is programming while the other person provides ideas.

R:I feel novel.

I:I have never heard of the concept of pair programming before. Today we not only recognized this concept, but also applied it to actual project development. In this programming approach, although I feel that the programming efficiency is slightly slower than programming alone, I am able to consider the business more comprehensively from the partner's reminder, thus writing more robust code. At the same time, I can also learn new knowledge in this programming method.

D:I hope to have the opportunity to try companion programming in the future and apply it to practical project development.